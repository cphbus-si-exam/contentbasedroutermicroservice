using System.Collections.Generic;

namespace Models
{
    public class CateringModel
    {
        public string eventid { get; set; }
        
        public int normalmeal { get; set; }
        public int specialmeal { get; set; }
        public List<string> type { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string request_type { get; set; }

    }
}