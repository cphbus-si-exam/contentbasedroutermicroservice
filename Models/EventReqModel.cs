using System.Collections.Generic;

namespace Models
{
    public class EventReqModel
    {
        public string request_type { get; set; }
        public bool confirm { get; set; }
        public string eventid { get; set; }
        public string sessionid { get; set; }
        public string eventtype { get; set; }
        public VenueModel venue { get; set; }
        public AccommodationModel accommodation { get; set; }
        public CateringModel catering { get; set; }
        public List<TransportModel> transport { get; set; }
    }
}