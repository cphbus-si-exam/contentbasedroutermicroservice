﻿using System;
using System.Text;
using CBRComponent.Models;
using Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;


namespace CBRComponent
{
    class Program
    {
        static void Main(string[] args)
        {
            var HostName = "dropletrabbit";
            var factory = new ConnectionFactory(){HostName=HostName};
            factory.Uri = new Uri(Environment.GetEnvironmentVariable("RABBIT_URI"), UriKind.Absolute);
            
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var ended = new ManualResetEventSlim();
                    channel.QueueDeclare("eventreq_queue",false, false, false, null);
                    channel.ExchangeDeclare("microservice_ex", ExchangeType.Direct);

                    channel.QueueDeclare("Agr_initPacket_queue",false, false, false, null);
                    channel.ExchangeDeclare("Agr_initPacket_ex",ExchangeType.Direct);
                    channel.QueueBind("Agr_initPacket_queue","Agr_initPacket_ex","");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model,ea) =>{

                        var message =  Encoding.UTF8.GetString(ea.Body);
                        var EventModel = ParseObject(message);
                        var initPacket = new InitPacketModel(){eventid=EventModel.eventid, sessionid=EventModel.sessionid, request_type=EventModel.request_type};

                        System.Console.WriteLine("----------------------------------------------------------------");
                        System.Console.WriteLine($"CBR RECEIVED : {message}");


                        //sending init packet to aggregator component
                        channel.BasicPublish("Agr_initPacket_ex","",body:Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(initPacket)));
                        System.Console.WriteLine("----------------------------------------------------------------");
                        System.Console.WriteLine("Init-packet sent " + JsonConvert.SerializeObject(initPacket));

                        

                        //putting Accommodation part of event into queue 
                        if( EventModel.accommodation != null){
                            var accmodel = EventModel.accommodation;
                            accmodel.eventid = EventModel.eventid;
                            accmodel.request_type = EventModel.request_type;
                            var accModelBA = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(accmodel));
                            channel.BasicPublish("microservice_ex", "Accommodation", body:accModelBA);
                            System.Console.WriteLine("----------------------------------------------------------------");
                            System.Console.WriteLine("Sending ACC model " + JsonConvert.SerializeObject(accmodel));
                        }

                        //putting Catering part of event into queue 
                        if( EventModel.catering != null){
                            var catmodel = EventModel.catering;
                            catmodel.eventid = EventModel.eventid;
                            catmodel.request_type = EventModel.request_type;
                            var catModelBA = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(catmodel));
                            channel.BasicPublish("microservice_ex", "Catering", body:catModelBA);
                            System.Console.WriteLine("----------------------------------------------------------------");
                            System.Console.WriteLine("Sending CAT model " + JsonConvert.SerializeObject(catmodel));
                        }

                        //putting transport part of event into queue 
                        if( EventModel.transport != null){
                            var tramodel = EventModel.transport;
                            foreach (var item in tramodel){
                                item.eventid=EventModel.eventid;
                                item.request_type = EventModel.request_type;
                            }
                            var traModelBA = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(tramodel));
                            channel.BasicPublish("microservice_ex", "Transport", body:traModelBA);
                            System.Console.WriteLine("----------------------------------------------------------------");
                            System.Console.WriteLine("Sending TRA model " + JsonConvert.SerializeObject(tramodel));
                        }
                        //putting venue part of event into queue 
                        if( EventModel.venue != null){
                            var venmodel = EventModel.venue;
                            venmodel.eventid = EventModel.eventid;
                            venmodel.request_type = EventModel.request_type;
                            var venModelBA = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(venmodel));
                            channel.BasicPublish("microservice_ex", "Venue", body:venModelBA);
                            System.Console.WriteLine("----------------------------------------------------------------");
                            System.Console.WriteLine("Sending VEN model " + JsonConvert.SerializeObject(venmodel));
                        }
                    };

                    channel.BasicConsume("eventreq_queue", true, consumer);
                    System.Console.WriteLine("Event request queue running ...");
                    ended.Wait();
                    System.Console.WriteLine("Shutting down...");
                }
            }
        }


        private static EventReqModel ParseObject(string jsonStr){
            var model = JsonConvert.DeserializeObject<EventReqModel>(jsonStr);
            return model;
        }

    }
}
